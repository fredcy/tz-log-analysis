# tz-log-analysis

Scrape data from Tezos logs

## Installation

```
poetry install
```
## Gather log data

Here is an example of getting the last four hours of tezos-node output, assuming that "tezos-node" is a systemd service that runs the tezos-node binary.

```
journalctl -u tezos-node --since "4 hour ago" > node4.log
```

## Run the program to extract validator times

```
poetry run python blocktime.py < node4.log
```

## Charting

I paste the output created above into Excel, then create a scatter chart with the timestamp column and one of the other columns
