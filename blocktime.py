from datetime import datetime
import re
import sys

pushed_pattern = re.compile(r"validator\.block: Request pushed")


def get_pushed_lines():
    for line in sys.stdin:
        m = pushed_pattern.search(line)
        if m:
            yield line.strip()


detail_pattern = re.compile(
    r"Request pushed on (.*?), treated in (.*?), completed in (.*?)(?: |$)"
)


def main():
    print("timestamp\ttreated\tcompleted")

    for line in get_pushed_lines():
        m = detail_pattern.search(line)
        if not m:
            sys.stderr.write(f"ERROR: line not matched: {line}\n")
            continue

        timestamp, treated, completed = m.groups()

        treated_ms = raw_duration_to_ms(treated)
        completed_ms = raw_duration_to_ms(completed)
        timestamp_excel = canon_date(timestamp)

        print(f"{timestamp_excel}\t{treated_ms:.3f}\t{completed_ms:.3f}")


duration_pattern = re.compile(r"(?:(\d+)min)?([\d.]+)(s|ms|us)$")

# conversion factor from given unit to milliseconds
unit_multiplier = {
    "min": 60 * 1000,
    "s": 1000,
    "ms": 1,
    "us": 0.001,
}


def raw_duration_to_ms(raw):
    # Convert duration string to milliseconds as float.
    # Typical input values:  "5.949s", "4.120us", "2.95ms", "2min39s"

    m = duration_pattern.match(raw)
    if not m:
        sys.stderr.write(f"ERROR: duration = '{raw}'\n")
        return "error"
    minutes, value, unit = m.groups()

    ms = int(minutes) * unit_multiplier["min"] if minutes else 0
    ms += float(value) * unit_multiplier[unit]

    # print(f"{raw} -> {ms}")
    return ms


def canon_date(raw):
    # Convert date string to a value that Excel understands. A typical raw value
    # looks like "2021-08-07T17:56:01.408-00:00".

    # arrange to ingore everything past the decimal point in the seconds value
    parts = raw.split(".")

    dt = datetime.strptime(parts[0], "%Y-%m-%dT%H:%M:%S")
    return dt.strftime("%Y-%m-%d %H:%M:%S")


main()
